public enum Mines {
    LITTLE(6),
    JUST(10),
    MANY(14);

    private final int number;
    private Mines(int number) {
        this.number = number;
    }
    public int get() {
        return number;
    }
}
