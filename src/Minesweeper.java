import java.security.InvalidParameterException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Minesweeper {
    public static void main(String[] args) {
        do {
            Game game = null;
            do {
                try {
                    game = new Game(setDifficultyLevel(getDifficultyLevel()));
                } catch (IllegalStateException | InvalidParameterException e) {
                    System.out.println(e.getMessage());
                } catch (InputMismatchException e) {
                    System.out.println("Unexpected value!");
                }
            } while (game == null);

            game.setGrid();
            game.placeMines();
            game.drawGrid();

            Result result = game.playGame();

            System.out.println(result == Result.WIN ? "Congrats! You won! :)" : "Mine! You lose! :(");
            System.out.println("Play again (Y/N)? ");
            System.out.print(": ");
        } while (Character.toLowerCase(new Scanner(System.in).next().charAt(0)) == 'y');
    }

    public static Difficulty getDifficultyLevel()
            throws InvalidParameterException,
                   InputMismatchException {
        System.out.println("\n### Minesweeper game ###");
        System.out.println("Choose difficulty level:");
        System.out.println("1. Easy");
        System.out.println("2. Medium");
        System.out.println("3. Hard");
        System.out.print(": ");

        return switch (new Scanner(System.in).nextInt()) {
            case 1 -> Difficulty.EASY;
            case 2 -> Difficulty.MEDIUM;
            case 3 -> Difficulty.HARD;
            default -> throw new IllegalStateException("Unexpected value!");
        };
    }

    public static Grid setDifficultyLevel(Difficulty difficulty) {
        Grid grid;
        return switch (difficulty) {
            case EASY -> grid = new Grid(Size.SMALL_W.get(), Size.SMALL_H.get(), Mines.LITTLE.get());
            case MEDIUM -> grid = new Grid(Size.MEDIUM_W.get(), Size.MEDIUM_H.get(), Mines.JUST.get());
            case HARD -> grid = new Grid(Size.LARGE_W.get(), Size.LARGE_H.get(), Mines.MANY.get());
        };
    }

}
