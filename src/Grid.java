public class Grid {
    public int gridWidth, gridHeight;
    public int numberOfMines;
    public boolean[][] mines, flagged, revealed;

    public Grid(int gridWidth, int gridHeight, int numberOfMines) {
        this.gridWidth = gridWidth;
        this.gridHeight = gridHeight;
        this.numberOfMines = numberOfMines;
    }

    // find adjacent mines (3x3 grid)
    public int calculateAdjacentMines(int x, int y) {
        if (isOutOfBounds(x, y)) {
            return 0;
        }
        int adjacentMines = 0;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                // do not exceed the bounds
                if (isOutOfBounds(i+x, j+y)) {
                    continue;
                }
                if (mines[i+x][j+y]) {
                    adjacentMines++;
                }
            }
        }

        return adjacentMines;
    }

    public void reveal(int x, int y) {
        // return case
        if (isOutOfBounds(x, y) || revealed[x][y]) {
            return;
        }
        revealed[x][y] = true;
        // return case
        if (calculateAdjacentMines(x, y) != 0) {
            return;
        }
        // reveal recursively
        reveal(x-1,y);      // middle left
        reveal(x+1,y);      // middle right
        reveal(x,y-1);      // middle top
        reveal(x,y+1);      // middle bottom
        reveal(x-1,y-1); // top left
        reveal(x+1,y-1); // top right
        reveal(x-1,y+1); // bottom left
        reveal(x+1,y+1); // bottom right
    }

    private boolean isOutOfBounds(int x, int y) {
        return x < 0 || y < 0 || x >= gridWidth || y >= gridHeight;
    }

}
// we're in the middle
// - - -
// - * -
// - - -
