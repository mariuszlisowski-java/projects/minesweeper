import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Game {
//    private final boolean MINES_VISIBILITY = false;
    private final boolean MINES_VISIBILITY = true;
    private final Grid grid;
    private boolean firstMove;

    public Game(Grid grid) {
        this.grid = grid;
        this.firstMove = true;
    }

    public void setGrid() {
        grid.mines = new boolean[grid.gridWidth][grid.gridHeight];
        grid.flagged = new boolean[grid.gridWidth][grid.gridHeight];
        grid.revealed = new boolean[grid.gridWidth][grid.gridHeight];
        clearGrid();
    }

    private void clearGrid() {
        for(int y = 0; y < grid.gridHeight; y++){
            for(int x = 0; x < grid.gridWidth; x++){
                grid.mines[x][y] = false;
                grid.flagged[x][y] = false;
                grid.revealed[x][y] = false;
            }
        }
    }

    public void placeMines() {
        int i = 0;
        Random random = new Random();
        while(i < grid.numberOfMines) {
            int x = random.nextInt(grid.gridWidth);
            int y = random.nextInt(grid.gridHeight);
            if (grid.mines[x][y]) {
                continue;
            }
            grid.mines[x][y] = true;
            i++;
        }
    }

    private void clearMines() {
        for (int x = 0; x < grid.gridWidth; x++) {
            for (int y = 0; y < grid.gridHeight; y++) {
                grid.mines[x][y] = false;
            }
        }
    }

    private void replaceMinesAgain(int x, int y) {
        System.out.println("* First move and mine hit! *");
        System.out.println("* New replacement of mines *");
        do {
            clearMines();
            placeMines();
        } while (grid.calculateAdjacentMines(x, y) != 0);
    }

    private Result flagCell(int x, int y) {
        // toggle flag
        grid.flagged[x][y] = !grid.flagged[x][y];

        if (isGameWon()) {
            return Result.WIN;
        }

        return Result.CONTINUE;
    }

    private Result hitCell(int x, int y) {
        // cell flagged?
        if (grid.flagged[x][y]) {
            grid.flagged[x][y] = false;
        }
        // mine hit?
        if (grid.mines[x][y]) {
            return Result.LOSE;
        }

        grid.reveal(x, y);

        return Result.CONTINUE;
    }

    private boolean isGameWon() {
        int count = 0;
        for(int y = 0; y < grid.gridHeight; y++) {
            for (int x = 0; x < grid.gridWidth; x++) {
                if (grid.flagged[x][y] && grid.mines[x][y]) {
                    count++;
                }
            }
        }

        return count == grid.numberOfMines;
    }

    public Result playGame() {
        Result result;
        do {
            int x = pickCellCoordinate("x");
            int y = pickCellCoordinate("y");
            // if the first hit is on a mine
            if (firstMove && grid.mines[x][y]) {
                replaceMinesAgain(x, y);
            }
            if (firstMove) firstMove = false;
            // flag or hit a cell
            if (isFlaggingPicked()) {
                result = flagCell(x, y);
            } else {
                result = hitCell(x, y);
            }
            drawGrid();
        } while (result == Result.CONTINUE);

        return result;
    }

    private int pickCellCoordinate(String coordinate) {
        int value = -1;
        int maxValue = 0;
        if (coordinate.equals("x")) maxValue = grid.gridWidth - 1;
        if (coordinate.equals("y")) maxValue = grid.gridHeight - 1 ;
        do {
            System.out.println("Choose a cell:");
            System.out.print(coordinate + " : ");
            try {
                value = new Scanner(System.in).nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Unexpected value!");
            }
            if (value < 0 || value > maxValue) {
                System.out.println("Out of bounds!");
            }
        } while (value < 0 || value > maxValue);

        return value;
    }

    private boolean isFlaggingPicked() {
        String str;
        do {
            System.out.print("(F)lag or hit enter : ");
            str = new Scanner(System.in).nextLine();
            if (str.isEmpty()) {
                return false;
            }
        } while (Character.toLowerCase(str.charAt(0)) != 'f');

        return true;
    }

    public void drawGrid() {
        printLineOfNumbers();
        for (int y = 0; y < grid.gridHeight; y++) {
            printColumnOfNumbers(y);
            printGrid(y);
        }
        System.out.println();
    }

    private void printLineOfNumbers() {
        System.out.print("\n     ");
        for (int i = 0; i < grid.gridWidth; i++) {
            String formatted = String.format("%02d", i);
            System.out.print(formatted + " ");
        }
        System.out.println();
    }

    private void printColumnOfNumbers(int y) {
        String formatted = String.format("%02d", y);
        System.out.print(formatted + "   ");
    }

    private void printGrid(int y) {
        for (int x = 0; x < grid.gridWidth; x++) {
            if (grid.flagged[x][y]) {
                System.out.print("M  ");
            } else if (grid.revealed[x][y]) {
                int minesCount = grid.calculateAdjacentMines(x, y);
                switch (minesCount) {
                    case 1 -> System.out.print("1  ");
                    case 2 -> System.out.print("2  ");
                    case 3 -> System.out.print("3  ");
                    case 4 -> System.out.print("4  ");
                    case 5 -> System.out.print("5  ");
                    case 6 -> System.out.print("6  ");
                    case 7 -> System.out.print("7  ");
                    default -> System.out.print("   ");
                }
            } else if (grid.mines[x][y]) {
                if (MINES_VISIBILITY) {
                    System.out.print("*  ");
                } else {
                    System.out.print("-  ");
                }
            } else {
                System.out.print("-  ");
            }
        }
        System.out.println();
    }

}
