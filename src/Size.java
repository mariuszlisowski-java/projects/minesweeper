public enum Size {
    SMALL_W(10),
    SMALL_H(6),

    MEDIUM_W(14),
    MEDIUM_H(10),

    LARGE_W(18),
    LARGE_H(14);

    private final int size;
    private Size(int size) {
        this.size = size;
    }
    public int get() {
        return size;
    }
}
